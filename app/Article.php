<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public $fillable = ['title', 'slug', 'preview', 'body'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function path()
    {
        return route('articles.show', $this);
    }
}
