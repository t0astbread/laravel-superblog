<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by TEMPLATED
http://templated.co
Released for free under the Creative Commons Attribution License

Name       : Undeviating 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20140322

-->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Superblog</title>
		<!-- <link
			href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900|Quicksand:400,700|Questrial"
			rel="stylesheet" /> -->
		<link href="/css/default.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/css/fonts.css" rel="stylesheet" type="text/css" media="all" />

	</head>
	<body>
		<div id="header-wrapper">
			<div id="header" class="container">
				<div id="logo">
					<span class="icon icon-cog"></span>
					<h1><a href="/">Superblog</a></h1>
				</div>
				<div id="menu">
					<ul>
						<li class="{{ Request::is('/') ? 'active' : '' }}"><a href="/" accesskey="1" title="">Homepage</a></li>
						<li class="{{ Request::is('about') ? 'active' : '' }}"><a href="/about" accesskey="2" title="About Us">About Us</a></li>
						<li class="{{ Request::is('articles') || Request::is('articles/*') ? 'active' : '' }}">
							<a href="{{ route('articles.index') }}" accesskey="3" title="Articles">Articles</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="wrapper" id="page-wrapper">
			@yield('content')
		</div>
		<div id="footer">
			<div class="container">
				<div class="fbox1">
					<span class="icon icon-map-marker"></span>
					<span>1234 Fictional Road Suite #789
						<br />Nashville TN 00000</span>
				</div>
				<div class="fbox1">
					<span class="icon icon-phone"></span>
					<span>
						Telephone: +1 800 123 4657
					</span>
				</div>
				<div class="fbox1">
					<span class="icon icon-envelope"></span>
					<span>businessname@business.com</span>
				</div>
			</div>
		</div>
		<div id="copyright">
			<p>&copy; Untitled. All rights reserved. | Photos by <a href="http://fotogrph.com/">Fotogrph</a> | Design by
				<a href="http://templated.co" rel="nofollow">TEMPLATED</a>.</p>
			<ul class="contact">
				<li><a href="#" class="icon icon-twitter"><span>Twitter</span></a></li>
				<li><a href="#" class="icon icon-facebook"><span></span></a></li>
				<li><a href="#" class="icon icon-dribbble"><span>Pinterest</span></a></li>
				<li><a href="#" class="icon icon-tumblr"><span>Google+</span></a></li>
				<li><a href="#" class="icon icon-rss"><span>Pinterest</span></a></li>
			</ul>
		</div>
	</body>
</html>
