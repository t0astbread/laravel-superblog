@extends('layout')

@section('content')
<h1>{{ $article->title }}</h1>
<a href="{{ URL::previous() }}">« Back</a>
<a href="{{ route('articles.edit', $article) }}">Edit</a>
<div class="container">
	<p>{{ $article->body }}</p>
</div>
@endsection
