@extends('layout')

@section('content')
<div class="container">
	<h1>Articles</h1>
	<p>These are our most well-written articles.</p>
	<a href="/articles/new">Write a new article</a>
</div>
<ul class="container">
	@foreach($articles as $article)
		<li><a href="{{ $article->path() }}">
			<h2>{{ $article->title }}</h2>
			<p>{{ $article->preview }}</p>
		</a></li>
	@endforeach
</ul>
{{ $articles->links() }}
@endsection
