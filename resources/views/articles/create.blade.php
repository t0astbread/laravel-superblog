@extends('layout')

@section('content')
<h1>New Article</h1>
<div class="container">
	<form action="{{ route('articles.store') }}" method="post">
		@csrf
		<label for="title">Title</label>
		<input type="text"
			name="title"
			id="title"
			class="@error('title') error @enderror"
			value="{{ old('title') }}">
		@error('title')
			<p>{{ $errors->first('title') }}</p>
		@enderror
		<label for="title">Preview</label>
		<input type="text" name="preview" id="preview">
		@error('preview')
			<p>{{ $errors->first('preview') }}</p>
		@enderror
		<label for="body">Body</label>
		<textarea name="body" id="body" cols="30" rows="10"></textarea>
		@error('body')
			<p>{{ $errors->first('body') }}</p>
		@enderror
		<button type="submit">Submit</button>
		<a href="{{ URL::previous() }}">Cancel</a>
	</form>
</div>
@endsection
