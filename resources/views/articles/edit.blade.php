@extends('layout')

@section('content')
<h1>New Article</h1>
<div class="container">
	<form action="{{ route('articles.update', $article) }}" method="POST">
		@csrf
		@method('PUT')

		<label for="title">Title</label>
		<input type="text" name="title" id="title" value="{{ $article->title }}">
		<label for="title">Preview</label>
		<input type="text" name="preview" id="preview" value="{{ $article->preview }}">
		<label for="body">Body</label>
		<textarea name="body" id="body" cols="30" rows="10">
			{{ $article->body }}
		</textarea>
		<button type="submit">Submit</button>
		<a href="{{ URL::previous() }}">Cancel</a>
	</form>
</div>
@endsection
