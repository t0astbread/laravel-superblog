#!/bin/sh
docker run --rm -d \
	-p 3306:3306 \
	-e MYSQL_USER=superblog \
	-e MYSQL_PASSWORD=superblogpass \
	-e MYSQL_RANDOM_ROOT_PASSWORD=yes \
	-e MYSQL_DATABASE=superblog \
	--name laravel-superblog \
	mariadb
